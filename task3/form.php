

<!DOCTYPE html>
<html lang="ru">
<head>
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Form PHP</title>
	<style>
	body {
		font:24pt sans-serif;
		text-align:center;
	}
	form {
		border-style: dashed;
		border-color: black;
		border-size:1px;
		font-size:12pt;
		padding:5px; 
		width: 400px;
		margin: 0 auto;
		text-align:center;
	}
	input.submit {
		width:60%;
	}
	input.text {
		width:90%;
	}
	select.text {
		width:90%;
	}
	textarea.text{
		width:90%;
	}
	</style>
</head>
<form action="" method="POST">
    <label>
      Имя: <br />
      <input name="name" class="text" type="text"/>
    </label><br />
    <label>
      Email:<br />
      <input name="email" class="text"
        value="sample@example.com"
        type="email" />
    </label><br />
    <label>
      Год рождения:<br />
      <select name="year">
   <?php for($i = 1900; $i < 2021; $i++) {?>
  	<option value="<?php print $i; ?>"><?= $i; ?></option>
  	<?php }?>
    </select>
    </label><br />
    Пол:<br />
    <label><input type="radio" checked="checked"
      name="sex" value="0" />
      М</label>
    <label><input type="radio"
      name="sex" value="1" />
      Ж</label><br />
    Количество конечностей:<br />
    <label><input type="radio" checked="checked"
      name="limb" value="1" />
      1</label>
    <label><input type="radio"
      name="limb" value="2" />
      2</label>
      <label><input type="radio"
      name="limb" value="3" />
      3</label>
      <label><input type="radio"
      name="limb" value="4" />
      4</label>   
	 <label><br/>
        Способности:
        <br />
        <select class="text" name="power[]"
          multiple="multiple">
          <option value="god">Бессмертие</option>
          <option value="clip">Прохождение сквозь стены</option>
          <option value="fly">Левитация</option>
        </select>
    </label><br />
    <label>
      Биография:<br />
      <textarea class="text" name="text" placeholder="Your biography" rows=10></textarea>
    </label><br />
    <label><input type="checkbox"
      name="check" required />
      С контрактом ознакомлен</label><br />
    <input type="submit" class="submit" value="Отправить" />
 </form> 
