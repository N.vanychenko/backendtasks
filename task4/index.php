<?php
header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();
  if (!empty($_COOKIE['save'])) {
    //print('Спасибо, результаты сохранены.');
    setcookie('save', '', 100000);
    $messages[] = '<div class="complete">Спасибо, результаты сохранены</div>';
  }
  // Включаем содержимое файла form.php.
  //$errors = FALSE;
$errors = array();
$errors['name'] = !empty($_COOKIE['name_error']);
$errors['email'] = !empty($_COOKIE['email_error']);
$errors['year'] = !empty($_COOKIE['year_error']);
$errors['power'] = !empty($_COOKIE['power_error']);
//ошибки в полях
if ($errors['name']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('name_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error1">Заполните имя</div>';
  }
if ($errors['email']) {
  if ($errors['email']==1){
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('email_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error1">Заполните почту</div>';
  }
  else {
     // Удаляем куку, указывая время устаревания в прошлом.
     setcookie('email_error', '', 100000);
     // Выводим сообщение.
     $messages[] = '<div class="error1">Неправильная почта</div>';
  }
  }
if ($errors['year']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('year_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error1">Заполните правильно год</div>';
  }
if ($errors['power']) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('power_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error1">Выберите суперспособность</div>';
  }
$values = array();
$values['name'] = empty($_COOKIE['name_value']) ? '' : $_COOKIE['name_value'];
$values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
$values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
$values['power'] = empty($_COOKIE['power_value']) ? '' : unserialize($_COOKIE['power_value']);
$values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
$values['limb'] = empty($_COOKIE['limb_value']) ? '' : $_COOKIE['limb_value'];
$values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];

  include('form.php');
}
else {
$errors = FALSE;

if (empty($_POST['name'])) {
  //print('Заполните имя.<br/>');
  setcookie('name_error', '1', time() + 24 * 60 * 60);
  $errors = true;
}
else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('name_value', $_POST['name'], time() + 30 * 24 * 60 * 60);
}

if (empty($_POST['email'])) {
  //print ('Заполните email. <br/>');
  setcookie('email_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
  //print ('Email введен некорректно. <br/>');
  setcookie('email_error', '2', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
}

if (empty($_POST['year'])) {
    //print('Заполните год.<br>');
    setcookie('year_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
}
else {
    $year = $_POST['year'];
    if (!(is_numeric($year) && intval($year) >= 1900 && intval($year) < 2020)) {
        //print("Укажите корректный год.<br>");
        setcookie('year_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
      // Сохраняем ранее введенное в форму значение на месяц.
      setcookie('year_value', $_POST['year'], time() + 30 * 24 * 60 * 60);
    }
}

$ability_data = ['god', 'clip', 'fly'];
if (empty($_POST['power'])) {
    //print('Выберите способность<br>');
    setcookie('power_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
}
else {
    $abilities = $_POST['power'];
    foreach ($abilities as $ability) {
        if (!in_array($ability, $ability_data)) {
            print('Недопустимая способность<br>');
            $errors = TRUE;
        }
    }
}
if ($errors==false) {
  setcookie('power_value', serialize($_POST['power']), time() + 30 * 24 * 60 * 60);
  setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
  setcookie('limb_value', $_POST['limb'], time() + 30 * 24 * 60 * 60);
  setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60); 
}

$ability_insert = [];
foreach ($ability_data as $ability) {
    $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
}



if($errors){
  // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('name_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('year_error', '', 100000);
    setcookie('power_error', '', 100000);
}
$user = 'task1user';
$pass = 'Task1pass!';
$db = new PDO('mysql:host=localhost;dbname=study', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
  $stmt = $db->prepare("INSERT INTO users (name,year,sex,email,bio,limb,ab_god,ab_fly,ab_clip) VALUES (:name,:year,:sex,:email,:bio,:limb,:ab_god,:ab_fly,:ab_clip)");
  $stmt -> bindParam(':name', $_POST['name']);
  $stmt -> bindParam(':year', $_POST['year']);
  $stmt -> bindParam(':sex', $_POST['sex']);
  $stmt -> bindParam(':email', $_POST['email']);
  $stmt -> bindParam(':bio', $_POST['bio']);
  $stmt -> bindParam(':limb', $_POST['limb']);
  $stmt -> bindParam(':ab_god', $ability_insert['god']);
  $stmt -> bindParam(':ab_fly', $ability_insert['fly']);
  $stmt -> bindParam(':ab_clip', $ability_insert['clip']);
  $stmt -> execute();
}
catch(PDOException $e) {
  print('Error : ' . $e->getMessage());
  exit();
}
setcookie('save', '1');

header('Location: index.php'); 
}
