<!DOCTYPE html>
<html lang="ru">
<head>
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Form PHP</title>
	<style>
	.error {
		border:2px solid red;
	}
  .error1 {
    border:2px solid red;
    font-size:14pt;
    padding: 2px;
    margin: 0px auto;
    margin-bottom:2px;
    width: 420px;
    text-align:center;
  }
	body {
		font:24pt sans-serif;
		text-align:center;
	}
  .complete {
    margin: 0px auto;
    width: 420px;
    border:2px solid green;
    font-size:14pt;
    text-align:center;
  }
	form {
		border-style: dashed;
		border-color: black;
		border-size:1px;
		font-size:12pt;
		padding:5px; 
		width: 400px;
		margin: 0 auto;
		text-align:center;
	}
	input.submit {
		width:60%;
	}
	input[name=name] {
		width:90%;
	}
  input[name=email] {
		width:90%;
	}
  input[name="power[]"] {
		width:90%;
	}
	select.text {
		width:90%;
	}
	textarea.text{
		width:90%;
	}
	</style>
</head>
<body>
<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
<form action="" method="POST">
    <label>
      Имя: <br />
      <input name="name" <?php if ($errors['name']) {print 'class="error"';} ?> value="<?php print $values['name']; ?>" type="text"/>
    </label><br />
    <label>
      Email:<br />
      <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"
        value="sample@example.com"
        type="text" />
    </label><br />
    <label>
      Год рождения:<br />
      <select name="year" <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year']; ?>">
      <option value="выбрать...">выбрать...</option>
   <?php for($i = 1900; $i < 2021; $i++) {?>
  	<option <?php if ($values['year']==$i){print 'selected="selected"';} ?> value="<?php print $i; ?>"><?= $i; ?></option>
  	<?php }?>
    </select>
    </label><br />
    Пол:<br />
    <label><input type="radio"
      name="sex" <?php if ($values['sex']==0){print 'checked';} ?> value="0" />
      М</label>
    <label><input type="radio" <?php if ($values['sex']==1){print 'checked';} ?>
      name="sex" value="1" />
      Ж</label><br />
    Количество конечностей:<br />
    <label><input type="radio" <?php if ($values['limb']==0 || $values['limb']==1){print 'checked';} ?>
      name="limb" value="1" />
      1</label>
    <label><input type="radio" <?php if ($values['limb']==2){print 'checked';} ?>
      name="limb" value="2" />
      2</label>
      <label><input type="radio" <?php if ($values['limb']==3){print 'checked';} ?>
      name="limb" value="3" />
      3</label>
      <label><input type="radio" <?php if ($values['limb']==4){print 'checked';} ?>
      name="limb" value="4" />
      4</label>   
	 <label><br/>
        Способности:
        <br />
        <select name="power[]" <?php if ($errors['power']) {print 'class="error"';} ?>
          multiple="multiple">
          <option <?php if (in_array("god",$values['power'])){print 'selected="selected"';} ?> value="god">Бессмертие</option>
          <option <?php if (in_array("clip",$values['power'])){print 'selected="selected"';} ?> value="clip">Прохождение сквозь стены</option>
          <option <?php if (in_array("fly",$values['power'])){print 'selected="selected"';} ?> value="fly">Левитация</option>
        </select>
    </label><br />
    <label>
      Биография (необязательно):<br />
      <textarea class="text" name="bio" placeholder="Your biography" rows=10><?php print $values['bio']; ?></textarea>
    </label><br />
    <label><input type="checkbox"
      name="check" required />
      С контрактом ознакомлен</label><br />
    <input type="submit" class="submit" value="Отправить" />
 </form> 
</body>
</html>